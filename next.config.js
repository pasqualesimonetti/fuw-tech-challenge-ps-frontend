/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    BACKEND_PATH: process.env.BACKEND_PATH,
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: '/index/smi',
        permanent: true,
      },
      {
        source: '/index/',
        destination: '/index/smi',
        permanent: true,
      },
      {
        source: '/company/',
        destination: '/company/novartis',
        permanent: true,
      },
    ]
  }
}

module.exports = nextConfig
