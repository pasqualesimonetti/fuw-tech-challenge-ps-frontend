import * as React from 'react';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import Link from '@mui/material/Link';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';

function checkDescription(params: GridValueGetterParams) {
  return params.row.description === 'NULL' ? '' : params.row.description;
}

const columns: GridColDef[] = [
  {
    field: 'name',
    headerName: 'Title',
    width: 150,
    renderCell: (params) => (
      <Link href={`/company/${params.row.slug}`}>{params.value}</Link>
    )
  },
  {
    field: 'isin',
    headerName: 'ISIN',
    width: 150
  },
  {
    field: 'description',
    headerName: 'Description',
    width: 250,
    valueGetter: checkDescription,
  }
];

// This gets called on every request
export async function getServerSideProps({ params }: any) {
  // Fetch data from external API
  const url = process.env.BACKEND_PATH+`/api/index/${params.indexSlug || 'smi'}`;
  const res = await fetch(url)
  const data = await res.json()
  const rows = data

  return { props: { rows } }
}

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const rpp = 10

export default function Listings({ rows }: any) {
  return (
    <Box sx={{
      display: 'flex',
      flexDirection: { xs: 'column', md: 'row' },
      alignItems: 'center',
      bgcolor: 'background.paper',
      overflow: 'hidden'
    }}
    >
      <Grid container spacing={12} >
        <Grid item xs={12}>
          <DataGrid
            sx={{ height: 108 + (rpp * 52) + 'px'}}
            getRowId={(row) => row.isin}
            rows={rows}
            columns={columns}
            pageSize={rpp}
            rowsPerPageOptions={[rpp]}
            disableSelectionOnClick
          />
        </Grid>
      </Grid>
    </Box>
  );
}
