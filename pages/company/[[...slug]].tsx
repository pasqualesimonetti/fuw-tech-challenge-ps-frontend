import * as React from 'react';
import TrendingUpIcon from '@mui/icons-material/TrendingUp';
import TrendingDownIcon from '@mui/icons-material/TrendingDown';
import Box from '@mui/material/Box';

// This gets called on every request
export async function getServerSideProps({ params }: any) {
  // Fetch data from external API
  const url = process.env.BACKEND_PATH+`/api/company/${params.slug || 'novartis'}`;
  const res = await fetch(url)
  const data = await res.json()

  return { props: { data } }
}

export default function Company({ data }: any) {
  return (
    <Box
      sx={{
        bgcolor: 'background.paper',
        boxShadow: 1,
        borderRadius: 1,
        p: 2,
        minWidth: 300,
      }}
    >
      <Box sx={{ color: 'text.secondary', fontSize: 14 }}>{`${new Date(data.dateLastTrade * 1000).toLocaleString()}`}</Box>
      <Box sx={{ color: 'text.primary', fontSize: 34, fontWeight: 'medium' }}>
        {data.name}
      </Box>
      <Box
        sx={{
          color: `${data.changePercent >= 0 ? 'success.dark' : 'error.dark'}`,
          display: 'inline',
          fontWeight: 'medium',
          mx: 0.5,
        }}
      >
        {data.priceNow.toFixed(2)}
      </Box>
      <Box
        component={ data.changePercent >= 0 ? TrendingUpIcon : TrendingDownIcon }
        sx={{fontSize: 16, verticalAlign: 'sub', color: `${data.changePercent >= 0 ? 'success.dark' : 'error.dark'}`}}
      />
      <Box
        sx={{
          color: `${data.changePercent >= 0 ? 'success.dark' : 'error.dark'}`,
          display: 'inline',
          fontWeight: 'medium',
          mx: 0.5,
        }}
      >
        { data.changePercent >= 0 ? '+' : '' }{data.changePercent}%
      </Box>
      <Box sx={{ color: 'text.secondary', display: 'inline', fontSize: 12 }}>
      ({ data.changePercent >= 0 ? '+' : '' }{data.changeAbsolute}) vs. {data.priceYesterday} yesterday
      </Box>
    </Box>
  );
}