FROM node:18.2
WORKDIR /home/node/app
COPY --chown=node:node . .
EXPOSE 3000
CMD [ "yarn", "start" ]